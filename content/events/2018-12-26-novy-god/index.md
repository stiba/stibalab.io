+++
title = "Новый год"
date = "2018-12-26"
image = "img/fronts/ng.png"
+++


Во всей нашей огромной стране ждут Новый год! Но только в детских садах этот
процесс ожидания такой наполненный, деятельный и энергичный. Мы ждали праздника
целый месяц. Решение о костюме приняли ещё в ноябре, в декабре много танцевали,
играли и обсуждали, какой же он всё-таки Дед Мороз.

И когда этот день всё-таки наступил, восторгу не было предела. Но вот какая
штука… На праздник к ребятам пришла волшебница Элла, она открыла секрет
волшебного ларца. Там хранятся удивительные шары, которые нужно открывать перед
каждым праздником. И без этого не наступит ни один праздник! Конечно, все ребята
предложили сразу открыть шар с Новогодним торжеством. Но… Элла обнаружила, что
кто-то перепутал шары в ларце, где они лежали строго по порядку и теперь, она не знает, в
каком именно спрятан Новый год!

Один из пап Андрей, предложил открывать все шары по порядку, пока не найдётся
нужный. Так и сделали! И по чистой случайности, Новогодний шар мы открыли
последним, зато перед этим совершили целое путешествие по таким праздникам как День
Рождения, День сказки, День защитника Отечества, 8 марта и даже День Осени. Из шаров
один из одним появлялись символы праздников, и Симка из детского мультфильма, и
Королева Осень.

По праздникам перемещались группы «Умка», «Колокольчики», «Капитошки»,
«Курносики» и «Веселые ребята».

Ребята из групп «Теремок», «Кроха», «Затейники» и «Ладушки» насладились
сказкой «О мышке-трусишке». Каждый понял, что если всегда сидеть под своим одеялом,
то никогда не узнаешь, сколько на свете всего интересного. Мышка-Малышка по совету
своей мамы и бабушки отправилась в гости к ребятам в детский сад, чтобы встретиться с
Дедом Морозом и Снегурочкой. На пути она забрела в Ледяную пещеру, где льдинки,
снежинки и снеговички играли для Мышки на треугольниках, побывала на волшебной
полянке, где живут феи и гномики, и даже не испугалась танцующих кошек. За такую
смелость и отвагу Дедушка Мороз подарил Мышке огромный кусок сыра. Ребята тоже
получили подарки от Дедушки, поиграли с ним и все вместе танцевали возле нарядной
ёлки. Вот такой вот смелый Новый год!

<iframe width="560" height="315" src="https://www.youtube.com/embed/3M6YWircyVY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<iframe width="560" height="315" src="https://www.youtube.com/embed/gRPvl8O6yUU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<iframe width="560" height="315" src="https://www.youtube.com/embed/odMS5KL9fmE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<iframe width="560" height="315" src="https://www.youtube.com/embed/6Sas4uyWUzs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<iframe width="560" height="315" src="https://www.youtube.com/embed/2c3PM_hbpwI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<iframe width="560" height="315" src="https://www.youtube.com/embed/MRt46Unbqwg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

![](1.jpg)
![](2.jpg)
![](3.jpg)
![](4.jpg)
![](5.jpg)
![](6.jpg)
![](7.jpg)
![](8.jpg)
