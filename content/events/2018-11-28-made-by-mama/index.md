+++
title = "«Made-by-mama»"
date = "2018-11-28"
image = "img/fronts/mama.png"
+++

**«Made-by-mama»** - именно под таким девизом прошёл марафон из 10 праздников в нашем детском саду. Можно сказать, что это было единение душ. Каждая мамочка вместе со своим ребёнком перевоплощалась в мастера по изготовлению музыкальных инструментов. Мы все попробовали быть коллегами великого Страдивари, и нужно сказать, что это безумно весело и интересно.

В группах «Умка» и «Колокольчики» при помощи лёгких манипуляций с иголкой и ниткой получились звенящие браслеты.

В группах «Веселые ребята», «Курносики», «Ладушки», «Карамельки»  и «Теремок» из обычного бумажного стаканчика получился уникальный маракас. Малыши с большой отдачей придумывали его дизайн, а мамы строго следили за количеством гречки внутри и аккуратностью выполнения рисунка.

Группа «Капитошки» преимущественна населена мальчишками, поэтому для них был выбран по-настоящему мужской инструмент – барабан. Контейнеры для хранения продуктов превратились в сам барабан, а палочки для кейк-попсов легко стали движущей силой музыки.

Самые младшие жители детского сада (группа «Кроха») с удовольствием отдали пальму первенства взрослым, ведь сами мамочки так увлеклись изготовлением маракаса из бутылочки, что даже потеряли счёт времени. От полученного результата малыши были в восторге! Каждый полюбил свой инструмент всем сердцем. А как же иначе, ведь он был сделан вместе с самым близким человеком на земле?

Во время совместного творчества поток теплой энергии был настолько большим, что хотелось, чтобы это продолжалось бесконечно. Это было единение душ! Мы надеемся, что такой формат мероприятий в нашем детском саду станет традиционным.

<iframe width="560" height="315" src="https://www.youtube.com/embed/rwXyXwsJNH8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<iframe width="560" height="315" src="https://www.youtube.com/embed/pS2dTi6G21o" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<iframe width="560" height="315" src="https://www.youtube.com/embed/4xdYBb-IvrE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<iframe width="560" height="315" src="https://www.youtube.com/embed/zPqPiL8-06c" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<iframe width="560" height="315" src="https://www.youtube.com/embed/1gwJ4-P-hys" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<iframe width="560" height="315" src="https://www.youtube.com/embed/A2okHmlxqQs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<iframe width="560" height="315" src="https://www.youtube.com/embed/iNv9oLbX-ME" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<iframe width="560" height="315" src="https://www.youtube.com/embed/g63oiTH32s4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<iframe width="560" height="315" src="https://www.youtube.com/embed/oyzH8UDmAak" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<iframe width="560" height="315" src="https://www.youtube.com/embed/rT8GPITFzWc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<iframe width="560" height="315" src="https://www.youtube.com/embed/kzkd4jLdbxY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<iframe width="560" height="315" src="https://www.youtube.com/embed/unbJtupBcz8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<iframe width="560" height="315" src="https://www.youtube.com/embed/-HslpsFbf0I" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<iframe width="560" height="315" src="https://www.youtube.com/embed/ALzm1M_1jT8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

![](1.jpg)
![](2.jpg)
![](3.jpg)
![](4.jpg)
![](5.jpg)
![](6.jpg)
![](7.jpg)
![](8.jpg)
![](9.jpg)
![](10.jpg)
![](11.jpg)
![](12.jpg)
![](13.jpg)
![](14.jpg)
![](15.jpg)
![](16.jpg)
