+++
title = "Городской конкурс любительского творчества «Голосок»"
date = "2018-10-20"
image = "img/fronts/golosok.jpg"
+++

20 октября 2018 года стал особенным днём для старшей группы девочек нашей вокальной студии. Эти крошки отправились покорять взрослую сцену. Не знаю, для кого это было более волнительно: для мам, бабушек, педагога или самих девочек. Но трепет и волнение перед выходом на сцену стало большим толчком для успешного выступления. Они так старались! И первый успех на заставил себя ждать. Второе место в городском конкурсе. Конечно, малышки не совсем поняли, что произошло и чем это выступление отличалось от обычного концерта. Но в целом это и не важно. Огромная гордость в глазах родителей, аплодисменты большого зала и желание петь ещё - вот, что стало настоящим призом, который девочки получили на этом конкурсе. Удачи им дальше!

<iframe width="560" height="315" src="https://www.youtube.com/embed/FE9OYYj3oU4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/5mp5wiewI70" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

![](1.jpg)
![](2.jpg)
![](3.jpg)
![](4.jpg)
![](5.jpg)
![](6.jpg)
![](7.jpg)
![](8.jpg)
