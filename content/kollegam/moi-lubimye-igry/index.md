+++
title = "Мои любимые игры"
date = "2018-09-01"
image = "img/fronts/games.png"
+++


## Это кто у нас хорошенький
Эта игра полезна не только детям в качестве психологического поглаживания, но она нужна и педагогам, которые очень хотят, но не знают, как проявить своё тепло по отношению к ребятам. Воспитатель, когда поёт про конкретного ребёнка, гладит его по голове. Так между ними создаётся мостик добра, который помогает не только во время музыкального воспитания, но и в целом в жизни детского сада.
<iframe width="560" height="315" src="https://www.youtube.com/embed/5Ik_YIPy0Gk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Здравствуйте, ручки (автор Ирина Галянт)
<iframe width="560" height="315" src="https://www.youtube.com/embed/unbJtupBcz8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Stop-and-go
Эта игра – настоящее чудо! Развивает так много всего, что даже не верится. Выбирается любая музыка, любой образ и происходит двигательная импровизация пока звучит музыка. Это может быть и игра на музыкальных инструментах, и игра в жуков (лежим на спинке и перебираем лапками). Как только музыка останавливается, нужно замереть. Так дети учатся соотносить движения с музыкой, быть внимательными, начинать и заканчивать движение вовремя, быть терпеливыми и выдерживать паузы.
<iframe width="560" height="315" src="https://www.youtube.com/embed/YCka4pJ7QNE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Роботы и звездочки (автор И.М. Каплунова)
Прелесть игры в том, что «Роботы и звездочки» совершенно условное название. Игра универсальна. В неё можно играть в любое время года, в любом составе, в любом возрасте. Модель проста: выбирается музыка в простой двухчастной форме, выбираются два разнохарактерных образа (феи и гномы, дети и великаны, цветы и бабочки и.т.д.), дети выбирают образ для себя и двигаются на свою музыку. В более старшем возрасте можно усложнить задачу и задать более конкретные параметры движениями, например, у каждой феи есть свой гномик, гномик ходит прямыми линиями, пока звучит музыка гнома, фея запоминает траекторию движения, и повторяет её только в более плавной интерпритации.
<iframe width="560" height="315" src="https://www.youtube.com/embed/UPYeD3K3V4s" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Топают все ножки у сороконожки (автор Т.Э. Тютюнникова)
Может легко заменить привычный вход в зал под марш. Позволяет развить ориентацию в пространстве, чувство «плеча друга».
<iframe width="560" height="315" src="https://www.youtube.com/embed/PQ6tzdPm5Lw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Здравствуйте (с именами)
<iframe width="560" height="315" src="https://www.youtube.com/embed/4kdrjjVlKAQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Между двух лесных дорог (автор Ирина Галянт)
<iframe width="560" height="315" src="https://www.youtube.com/embed/KJOA6Vpql1Y" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Фермер (автор Ирина Галянт)
Эта игра уникальна, потому что готовит нас к неожиданным поворотам судьбы. Да-да, простая детская игра формирует такое качество, как быстрота реакции и способность оперативно что-то придумать. А разве не это нужно нам жизни несколько больше, чем умение выполнять поскоки под музыку? Так вот, рассказывается история о том, что мы сейчас поедем на ферму и повезёт нас Миша. Все цепляются друг за друга, а Миша становится первым. Пока играет музыка Миша везёт всех на ферму, как только первая часть музыки заканчивается, Миша показывает любое танцевальное движение, которое все за ним повторяют. Представляете, как трудно сообразить, когда все на тебя смотрят и чего-то ждут?! Так вот, мы этому и учимся! Учимся не бояться внимания и быстро принимать решения.
<iframe width="560" height="315" src="https://www.youtube.com/embed/ALzm1M_1jT8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
