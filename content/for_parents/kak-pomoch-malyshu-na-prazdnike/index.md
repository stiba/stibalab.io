+++
title = "Как помочь малышу на празднике"
date = "2018-12-31"
image = "img/fronts/pomoch-malyshu.png"
+++


1. Помните, что вы прежде всего не зритель, а помощник. Хлопайте всегда! Если сомневаетесь, пора ли сейчас хлопать, хлопайте! Лучше перехлопать, чем недохлопать.
2. Воспоминания – это очень важно, и бабушке Любе из Комсомольска тоже хочется посмотреть, каким милым волчонком был ваш малыш. Но если вы будете смотреть праздник только через экран телефона, то можете пропустить что-нибудь важное. Например, вашу очередь петь или выходить танцевать. Поэтому, может стоит договориться с другими родителями о видеосъёмке? Так ваши руки смогут вовремя застегнуть сандалик или поправить крылья у бабочки.
3. Радость для себя мы создаём сами! На празднике от родителей мы просим лишь хорошее настроение. Когда вы заходите в зал, улыбнитесь, и тогда вам точно понравится.
